FROM golang:1.20-bullseye

ARG REPO_URL
ENV CODE_REPO $REPO_URL
ENV USER_TO_EXEC=builder

RUN apt-get update && \
    useradd -ms /bin/bash ${USER_TO_EXEC}

USER ${USER_TO_EXEC}
WORKDIR /home/${USER_TO_EXEC}

RUN git clone ${CODE_REPO} && \
    cd rest-cpu-load && \
    make build && \
    chmod +x build/rest-cpu-load

USER root

RUN ln -s /home/${USER_TO_EXEC}/rest-cpu-load/build/rest-cpu-load /usr/local/bin/
USER builder

EXPOSE 8080

CMD ["rest-cpu-load"]